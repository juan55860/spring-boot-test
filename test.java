package com.codility.tasks.hibernate.solution;

import org.springframework.data.jpa.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.*;
import org.springframework.web.bind.annotation.*;

import javax.persistence.*;
import java.util.logging.Logger;

@Entity
@Table(name = "task")
class Task {
  private Long id;
  private String description;
  private Long priority;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
      public long getId() {
      return id;
  }

  @Column(name = "description", nullable = false)
  public String getDescription() {
      return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Column(name = "priority", nullable = false)
  public Long getPriority() {
      return this.priority;
  }

  public void setPriority(Long priority) {
    this.priority = priority;
  }
}

@RestController
@RequestMapping("/tasks")
class TaskController {
   private TaskRepository taskRepository;
  //private static Logger log = Logger.getLogger("Solution");

  @PutMapping(value = "/{id}")
  public ResponseEntity<Task> update(@PathVariable(value = "id") Long taskId) throws ResourceNotFoundException {
		Task task = taskRepository.findById(taskId).orElseThrow(() -> new ResourceNotFoundException("Task not found for this id :: " + taskId));
		if (task != null) {
			// task.delete(id);
		} else {
			return new ResponseEntity<Task>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}

  // log.info("You can use 'log' for debug messages");
}

interface TaskRepository extends JpaRepository<Task, Long> {

}

interface TaskService {
  public Task update(Task task);
}


@ResponseStatus(value = HttpStatus.NOT_FOUND)
class ResourceNotFoundException extends Exception{

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String message){
        super(message);
    }
}
